import React from "react"
import { Grid, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Header from "./header";

const Layout = ({ children }) => {

  const drawerWidth = 240;
  const useStyles = makeStyles((theme) => ({
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: drawerWidth
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }));

  const classes = useStyles();

  return (
    <>
      <Header>
        <Container>
          <Grid container spacing={2} direction="column">
            <div className={classes.paper}>
              {children}
            </div>
          </Grid>
        </Container>
      </Header>
    </>
  );
}


export default Layout

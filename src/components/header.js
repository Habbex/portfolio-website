import React from 'react';
import { Link, useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import List from '@material-ui/core/List';
import ThemeSwitcher from "../components/themeSwitcher";
import GitHubIcon from '@material-ui/icons/GitHub';
import TwitterIcon from '@material-ui/icons/Twitter';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import InfoIcon from '@material-ui/icons/Info';
import CategoryIcon from '@material-ui/icons/Category';
import CollectionsBookmarkIcon from '@material-ui/icons/CollectionsBookmark';
import Hidden from '@material-ui/core/Hidden';
import { CssBaseline, Grid, Container } from '@material-ui/core';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex'
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    }
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  closeMenuButton: {
    marginRight: 'auto',
    marginLeft: 0
  },
  title: {
    flexGrow: 1,
    textDecoration: 'none',
    textAlign: 'center',
    padding: theme.spacing(1)
  },
  link: {
    textAlign: 'center',
    textDecoration: 'none',
    color: theme.palette.text.secondary,
  },
  activeLink: {
    color: theme.palette.text.disabled,
    textAlign: 'center',
    textDecoration: 'none',
  },
  imageAvatar: {
    margin: theme.spacing(2),
    boxSizing: "inherit",
    textAlign: "center",
    display: "inline-block",
    position: "relative",
    width: "10.25em",
    border: `2px solid ${theme.palette.text.secondary}`,
    borderRadius: "100%",
  },
  imageAvatarHorizontallyCenter: {
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)'
  },
  drawerContainer: {
    paddingTop: "100px"
  },
}));


const Header = ({ children }) => {
  const data = useStaticQuery(graphql`
    query {
      profileImg: file(relativePath: { eq: "images/header/profilePic.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 300) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  const classes = useStyles();
  const [mobileOpen, setMobileOpen] = React.useState(false)

  const drawer = (
    <div className={classes.drawerContainer}>
      <List>
        <Link to="/blogs">
          <ListItem>
            <div className={classes.imageAvatarHorizontallyCenter}>
              <Img
                className={classes.imageAvatar}
                fluid={data.profileImg.childImageSharp.fluid}
              />
            </div>
          </ListItem>
        </Link>
        <div className={classes.drawerContainer}>
          <ListItem>
            <ListItemText primary="I do computer magic 🧙‍♂️✨" />
          </ListItem>
          <Link activeClassName={classes.activeLink} className={classes.link} to="/blogs">
            <ListItem button >
              <ListItemText primary="Blog posts" />
              <ListItemIcon><CollectionsBookmarkIcon /></ListItemIcon>
            </ListItem>
          </Link>
          <Link activeClassName={classes.activeLink} className={classes.link} to="/projects">
            <ListItem button >
              <ListItemText primary="Projects" />
              <ListItemIcon><CategoryIcon /> </ListItemIcon>
            </ListItem>
          </Link>
          <Link activeClassName={classes.activeLink} className={classes.link} to="/contact-me">
            <ListItem button >
              <ListItemText primary="Contact Me" />
              <ListItemIcon><ContactMailIcon /> </ListItemIcon>
            </ListItem>
          </Link>
          <Link activeClassName={classes.activeLink} className={classes.link} to="/about-me">
            <ListItem button >
              <ListItemText primary="About Me" />
              <ListItemIcon><InfoIcon /> </ListItemIcon>
            </ListItem>
          </Link>
          <ListItem>
            <ThemeSwitcher />
            <ListItemText primary="Theme switcher" />
          </ListItem>
          <ListItem>
            <ListItemIcon><GitHubIcon /> <MailIcon /> <TwitterIcon /></ListItemIcon>
          </ListItem>
        </div>
        <Divider />
      </List>
    </div>
  )

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton color="inherit"
            aria-label="Open drawer"
            edge="start"
            onClick={() => setMobileOpen(!mobileOpen)}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap align="center">
            <span role="img" aria-label="earth">Hello world 🌍</span> 
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer}>
        <Hidden smUp implementation="css">
          <Drawer variant="temporary"
            open={mobileOpen}
            onClose={() => setMobileOpen(!mobileOpen)}
            className={classes.drawer}
            anchor='left'
            classes={{ paper: classes.drawerPaper }}
            ModalProps={{ keepMounted: true }}>
            <IconButton onClick={() => setMobileOpen(!mobileOpen)} className={classes.closeMenuButton}>
              <CloseIcon />
            </IconButton>
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer className={classes.drawer} variant="permanent" classes={{ paper: classes.drawerPaper }}>
            <div className={classes.toolbar} />
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <div className={classes.content}>
        <div className={classes.toolbar} />
        <Container>
          <Grid container direction="column">
            <div className={classes.paper}>
              {children}
            </div>
          </Grid>
        </Container>
      </div>
    </div>
  );
}

export default Header